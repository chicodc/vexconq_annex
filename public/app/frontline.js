//'use strict';


var app = angular.module('Frontline', ['ngAnimate','ui.bootstrap', 'ngCookies']);


app.controller('FrontlineController',['$scope','$rootScope','$route', '$q', '$timeout', '$compile', '$uibModal','$log', '$routeParams', '$cookies', '$location', 'generalFactory', 'socket',
	function( $scope, $rootScope, $route, $q, $timeout, $compile, $uibModal, $log, $routeParams, $cookies, $location, generalFactory, socket,){

	$scope.menu = "";
	$scope.account = "";
	$scope.counter;
	$scope.called = false;
	$scope.clickedNext = false;

	$scope.timeoutContainer;

	$scope.startTime = function(){
		$scope.$parent.startTime();
	};
	$scope.stopTime = function(){
		$scope.$parent.stopTime();
	};

	function initfrontcall(){
		$scope.called = false;
		if($scope.$parent.available === 1){
			var timeout = $timeout(function () {
				$(".pageheader").toggle("blind");
			});
			timeout.then(function(){
				$timeout.cancel(timeout);
			}, function(){console.log('Timer destroyed', Date.now());});
		}

		var allFrontPromises = $q.all([
			$scope.getAllPending(),
			$scope.getAllSkipped(),
			$scope.getAllServed(),
			$scope.getCurrent()
		]);

		allFrontPromises.then(function(){
			$scope.timeoutContainer = $timeout(function(){
				$('#loading_screen').fadeOut();

				/*if($scope.current_client != 'NONE' || $scope.jumpnumber != 0){
					$scope.$parent.seconds = $cookies.get('lastSeconds');
					$scope.$parent.minutes = $cookies.get('lastMinutes');
					$scope.$parent.hours = $cookies.get('lastHours');

					if($scope.called){
						$scope.$parent.startTime();
					}
				}*/
			}, 3000, false);

			$scope.timeoutContainer.then(function(){
				$timeout.cancel($scope.timeoutContainer);
			}, function(){
				console.log('Timer destroyed', Date.now());
			});
		});
	};

	socket.on('newPendingAdded', function(){
		//socket.getSocket().removeAllListeners();
		//clear all pending interval
		$scope.$parent.stopTime();
		initfrontcall();
	});

	socket.on('arisready', function(){
		socket.getSocket().removeAllListeners();
		$scope.initfront();
	});

	$scope.getAllPending = function(){
		generalFactory.getHandler('/frontline/pending/getall', function(response){
				$scope.pending = response.pending;
				$scope.pending_count = response.pending_count.count;
		});
	};

	$scope.getAllSkipped = function(){
		generalFactory.getHandler('/frontline/skipped', function(response){
				$scope.skipped = response.skipped;
				$scope.skipped_count = response.skipped_count.count;
		});
	};

	$scope.getAllServed = function(){
		generalFactory.getHandler('/frontline/served', function(response){
				$scope.served = response.served;
				$scope.served_count = response.served_count.count;
		});
	};

	$scope.getCurrent = function(){
		generalFactory.getHandler('/frontline/currentskipped', function(response){
				$scope.current_skipped = response.current_skipped;
				$scope.jumpnumber = response.current_skipped.numberque;
				if($scope.jumpnumber != 0){
					$scope.skipped_time_called = response.current_skipped[0].time_called;
				}

				generalFactory.getHandler('/frontline/current', function(response){
					$scope.current_client = response.current;
					if($scope.current_client != 'NONE'){
						$scope.current_time_called = response.current[0].time_called;
					}

					if($scope.current_client == 'NONE' && $scope.jumpnumber == 0){
						$scope.buts1 = true;
						$scope.buts2 = false;
					}else{
						$scope.buts1 = false;
						$scope.buts2 = true;
					}

					if(!$scope.skipped_time_called && !$scope.current_time_called){
						$scope.called = false;
					}else{
						$scope.called = true;
					}

					console.log($scope.skipped_time_called, $scope.current_time_called);
				});
			});
	};

	$scope.back = function(){
		$location.path('/menu');
	};

	$scope.initfront = function(){
		initfrontcall();
	};

	$scope.next = function(){
		if(!$scope.$parent.counter){
			alert('Please set your counter number first.');
		}else{
			if($scope.pending_count === 0){
				alert('There are no pending client.');
			}else{
				$scope.clickedNext = true;
				generalFactory.getHandler('/frontline/next', function(response){
					if(response.success === 'NONE'){
						$scope.clickedNext = false;
					}else{
						if(response.success === 'ACTIVE'){
							alert(response.message);
						}else{
							$scope.clickedNext = false;
							$scope.getAllPending();
							$scope.getAllSkipped();
							$scope.getAllServed();
							$scope.getCurrent();
						}
					}
				});
			}
		}
	};
	
	$scope.openmodal = function (size, url) {
    	var modalInstance = $uibModal.open({
	      	animation: true,
	      	templateUrl: url,
	      	controller: 'AddUserController',
	      	size: size,
	      	scope: $scope,
	      	windowClass: 'app-modal-window',
	      	resolve: {
	        		items: function () {
	          		return $scope;
	        	}
	     	}
	    });

	    modalInstance.result.then(function (selectedItem) {
	      	$scope.selected = selectedItem;
	    }, function () {
	      	$log.info('Modal dismissed at: ' + new Date());
	    });
	};


	$scope.serve_client = function(jumpnumber){
		var alias = $scope.alias;
		
		$scope.clickedNext = false;
		generalFactory.getHandlerExtra('/frontline/serve/' + jumpnumber, alias, function(response){
			if(response.success == "UNPREPARED"){
				alert('Please PREPARE the AR first');
			}else{
				$scope.$parent.stopTime();
				$scope.$parent.resetTime();

				$scope.called = false;
				socket.emit('refreshQuetv');
				$scope.getAllPending();
				$scope.getAllSkipped();
				$scope.getAllServed();
				$scope.getCurrent();
			}
			console.log("====ALIAS VALUE=====");
			console.log(alias);
			console.log("====ALIAS VALUE=====");
			$route.reload();
		});
	};

	$scope.serve_clientExtra = function(){
		$scope.openmodal('', 'partials/form_InputRemarks.html');
	};

	$scope.call_client = function(){
		//generalFactory.getHandler('/frontline/updateCounter', function(response){});
		generalFactory.getHandler('/frontline/call', function(response){
			if(response.success != 'ERROR'){
				socket.emit('callClient', {counter : response.counter});
				$scope.called = true;

				/*$cookies.remove('clock');
				$cookies.put('clock', true);
				$scope.$parent.resetTime();
				$scope.$parent.startTime();*/
				$scope.$broadcast('callname');
			}
		});
	};

	$scope.call_only = function(counterNo){
		generalFactory.getHandler('/frontline/myCounter', function(response){
			socket.emit('callClient', {counter : response.counter});
			$scope.$broadcast('callname');
		});
	};

	$scope.skip_client = function(){
		$scope.clickedNext = false;
		generalFactory.getHandler('/frontline/skip', function(response){
			if(response.success != 'SKIPPED'){

				$scope.$parent.stopTime();
				$scope.$parent.resetTime();

				$scope.called = false;
				socket.emit('refreshQuetv');
				$scope.getAllPending();
				$scope.getAllSkipped();
				$scope.getAllServed();
				$scope.getCurrent();
			}else{
				alert('This client was already skipped! Sorry, you cannot skip a client twice.');
			}
		});
	};

	$scope.jump = function(){
		if($scope.skipped_count === 0 ){
			alert('There are no skipped clients.');
		}else{
			$scope.$parent.openM('' , 'partials/form_jump.html');
		}
	};

	$scope.printServed = function(){
		$("#skippedList").printThis({
		    debug: false,
		    importCSS: true,
		    importStyle: true,
		    printContainer: true,
		    loadCSS: ["http://10.0.1.2:8080/bootstrap/dist/css/bootstrap.min.css",
		    		  "http://10.0.1.2:8080/stylesheets/quelogin.css",
		    		  "http://10.0.1.2:8080/stylesheets/sbadmintheme/css/style-override.css"],
		    pageTitle: "My Modal",
		    removeInline: false,
		    printDelay: 333,
		    header: null,
		    formValues: true
		});
	};

	$scope.$on('refreshFront', function(){
		$scope.clickedNext = false;
		$scope.getAllPending();
		$scope.getAllSkipped();
		$scope.getAllServed();
		$scope.getCurrent();
	});

	$scope.refresh = function(){
		socket.emit('refreshQuetv');
	};


    $scope.$on('jumped', function(){
    	$scope.clickedNext = true;
    });

}]);

app.controller('QuelistController',['$scope','$rootScope','$route', '$q', '$timeout', '$compile', '$uibModal','$log', '$routeParams', '$cookies', '$location', 'generalFactory', 'socket',
	function( $scope, $rootScope, $route, $q, $timeout, $compile, $uibModal, $log, $routeParams, $cookies, $location, generalFactory, socket){

	$scope.initList = function(){
		$scope.getAllPending();
		$scope.getStatus();
	};

	$scope.getAllPending = function(){
		generalFactory.getHandler('/frontline/pending/getall', function(response){
				$scope.pending = response.pending;
				$scope.pending_count = response.pending_count.count;
		});
	};

	$scope.getStatus = function(){
		generalFactory.getHandler('/main/getServices', function(response){
			if(response.status === "SUCCESS"){
				$scope.services = response.result;
			}
		});
	};

	socket.on('newPendingAdded', function(){
		$scope.getAllPending();
	});

}]);


app.directive('clientname', ['$timeout', function($timeout){
	return {
		link : function($scope, element, attrs){
			$scope.$on('callname', function (event, args){
				$('.servename').blink({
						maxBlinks: 3,
						speed: 700,
				});
				$('#call_note').hide();
			});
		}
	};
}]);

/*FUNCTION TO STICK THE PAGEHEADER IN PLACE*/
/*app.directive('frontliner', ['$timeout', function ($timeout) {
  return {
    link: function ($scope, element, attrs) {
      $scope.$on('dataloaded', function (event, args) {
      	 $timeout(function () {
      		$(window).scroll(function(e){
			var distanceFromTop = $(document).scrollTop();
        	var pageheaderheight = $('.forpagehead').height();
        	var breadcrumbsheight = $('.breadcrumbs').height();
        	var combined = pageheaderheight + breadcrumbsheight;

        	if (distanceFromTop >= $('.navbar').height()){
		     	$('.forpagehead').css({'height': pageheaderheight, 'position': 'fixed', 'z-index': '132'}).fadeIn();
		     	$('.buts').css({'right': '19.5%'});
		     	$('.breadcrumbs').css({'position': 'fixed', 'z-index': '99', 'width' : '100%'}).fadeIn();
		  	}else{
		  	 	$('.forpagehead').css({'position': '', 'z-index': ''}).fadeIn();
		     	$('.buts').css({'right': '5px'});
		     	$('.breadcrumbs').css({'position': '', 'z-index': '', 'width' : ''}).fadeIn();
		  	}
		});

      }, 0, false);
      });
    }
  };
}]);*/
